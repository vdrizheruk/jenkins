FROM ubuntu:14.04

ENV TERM=xterm

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y git htop
